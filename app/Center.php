<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Center extends Model
{
    /**
     * @var string
     */
    protected $table = 'organizations.center';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'assessment_center', 'cluster_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cluster()
    {
        return $this->belongsTo(Cluster::class, 'cluster_id');
    }
}
