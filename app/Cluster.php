<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cluster extends Model
{
    /**
     * @var string
     */
    protected $table = 'organizations.cluster';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function school(){
        return $this->hasMany(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function center(){
        return $this->hasMany(Center::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id');
    }
}
