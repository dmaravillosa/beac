<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    /**
     * @var string
     */
    protected $table = 'organizations.division';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function school(){
        return $this->hasMany(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id');
    }
}
