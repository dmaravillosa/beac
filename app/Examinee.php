<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use App\Traits\Encryptable;

class Examinee extends Model
{
//    use Encryptable;

    /**
     * @var array
     */
//    protected $encryptable = [
//        'last_name',
//        'first_name',
//        'middle_name',
//        'lrn',
//        'birthdate',
//    ];

    /**
     * @var string
     */
    protected $table = 'records.examinee';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lrn', 'last_name', 'first_name', 'middle_name', 'birthdate', 'gender', 'school_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school(){
        return $this->belongsTo(School::class, 'school_id');
    }
}
