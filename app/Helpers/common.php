<?php

/**
 * Disable html5 validation
 * @return string
 */
function disableValidateIfTesting()
{
    if (config('app.env') == 'local') return 'novalidate';
    return '';
}

?>