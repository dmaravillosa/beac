<?php

/**
 * Default storage app
 * @return string
 */
function storage_app()
{
    return storage_path('app/');
}

?>