<?php

/**
 * Generate unique ID
 *
 * @return string
 */
function uniq_id(){
    return (time() + random_int(100000, 999999)) * random_int(2,5)  . '_'
        . (time() + random_int(100000, 999999)) * random_int(2,5) . '_'
        . (time() + random_int(100000, 999999)) * random_int(2,5) . str_shuffle(time()) . random_int(1000, 9000);
}

function temporary_password(){
    return substr(md5(microtime()),rand(0,26),5);
}

function school_type($indexes = []){

    $schoolType = "";

    for($x = 0; $x < count($indexes); $x++){

        switch ($indexes[$x]){
            case "1":
                $schoolType .= 'Kinder';
                break;
            case "2":
                $schoolType .= 'Grade 1-6';
                break;
            case "3":
                $schoolType .= 'Grade 7-10';
                break;
            case "4":
                $schoolType .= 'Grade 11-12';
                break;
        }

        if(!$x == count($indexes) - 1)
            $schoolType .= ', ';

    }

    return $schoolType;
}

/**
 * Encrypt and decrypt
 *
 * @author Nazmul Ahsan <n.mukto@gmail.com>
 * @link http://nazmulahsan.me/simple-two-way-function-encrypt-decrypt-string/
 *
 * @param string $string string to be encrypted/decrypted
 * @param string $action what to do with this? e for encrypt, d for decrypt
 * @return bool|string
 */
function custom_crypt($string, $action = 'e')
{
    $key = 'usV4rsdlRoTWea4VGaXC';
    $iv = 'usV4rsdlRoxIv5h5FsF6';

    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash('sha256', $key);
    $iv = substr(hash('sha256', $iv), 0, 16);

    if ($action == 'e')
        $output = base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
    else if ($action == 'd')
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);

    return $output;
}

?>