<?php

namespace App\Http\Controllers\Auth;

use App\Mail\TemporaryPassword;
use App\Region;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['role:DEPED_ADMIN']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'role' => 'required',
            'first_name' => 'required|string|max:255',
            'middle_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:pgsql.users.user',
            'designation' => 'required|string|max:255',
            'school_type' => 'required|max:255',
            'classification' => 'required|string|in:Public,Private',
            'region_access' => 'required|max:255',
            'division_access' => 'required|string|max:255'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if($data['role'] == '0')
            $role = config('constants.DEPED_ADMIN');
        else if($data['role'] == '1')
            $role = config('constants.DIVISION_SUPERVISOR');
        else
            $role = config('constants.SERVICE_PROVIDER_STAFF');

        //Generate random character temporary password
        $temp_password = temporary_password();

        $user = User::create([
            'first_name' => $data['first_name'],
            'middle_name' => $data['middle_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($temp_password),
            'designation' => $data['designation'],
            'school_type' => school_type($data['school_type']),
            'classification' => $data['classification'],
            'region_access' => Region::where('id', $data['region_access'] + 1)->pluck('code')[0],
            'division_access' => $data['division_access'],
            'created_by' => 'Created by: ' . Auth::user()->first_name . ' ' . Auth::user()->last_name,
        ])->assignRole($role);

        Mail::to($user)->send(new TemporaryPassword($user, $temp_password));

        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return redirect()->action('UsersController@index');
    }
}
