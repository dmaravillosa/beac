<?php

namespace App\Http\Controllers;

use App\Http\Requests\CenterRequest;
use App\Repositories\CenterRepository;
use Illuminate\Http\Request;


class CentersController extends Controller
{
    /**
     * View directory
     * @var string
     */
    protected $view = 'pages.admin.assessment_center';

    /**
     * @var CenterRepository Repository
     */
    protected $centers;

    /**
     * CentersController constructor.
     * @param CenterRepository $centerRepository
     * @param Request $request
     */
    function __construct(CenterRepository $centerRepository, Request $request)
    {
        $this->middleware(['role:DEPED_ADMIN']);
        $this->centers = $centerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->view);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function data(){
        return $this->centers->getAllData();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CenterRequest $request)
    {
        $data = $request->except(['_token', '_method', 'id']);

        if($this->centers->addCenter($data)){
            alert()->success("Adding assessment center successful", 'Success!')->persistent('Close');
        }else{
            alert()->error("Something went wrong in adding the assessment center", 'Oops!')->persistent('Close');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CenterRequest $request)
    {
        $data = $request->except(['_token', '_method']);

        if($this->centers->updateCenter($data)){
            alert()->success("Updating assessment center successful", 'Success!')->persistent('Close');
        }else{
            alert()->error("Something went wrong in updating the assessment center", 'Oops!')->persistent('Close');
        }

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus($id)
    {
        if ($this->centers->updateStatus($id)){
            return response()->json(['result' => 'success']);
        }
        return response()->json(['result' => 'failed']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->centers->deleteCenter($id))
            return response()->json(['result' => 'success']);

        return response()->json(['result' => 'failed']);
    }
}
