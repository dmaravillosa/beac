<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExamineeRequest;
use App\Repositories\ExamineeRepository;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class ExamineeController extends Controller
{
    /**
     * @var string
     */
    public $view = 'pages.admin.examinees';

    /**
     * @var ExamineeRepository
     */
    public $examinees;

    /**
     * ExamineeController constructor.
     * @param ExamineeRepository $examineeRepository
     */
    public function __construct(ExamineeRepository $examineeRepository)
    {
        $this->middleware(['role:DIVISION_SUPERVISOR|DEPED_ADMIN']);
        $this->examinees = $examineeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->view);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function data(){
        return $this->examinees->getAllData();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param ExamineeRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ExamineeRequest $request)
    {
        $data = $request->except(['_token', '_method', 'id']);

        if($this->examinees->addPeriod($data)){
            alert()->success("Adding data successful", 'Success!')->persistent('Close');
        }else{
            alert()->error("Something went wrong in adding the data", 'Oops!')->persistent('Close');
        }

        return redirect()->back();
    }

    /**
     * @param $id
     */
    public function show($id)
    {
        //
    }

    /**
     * @param $id
     */
    public function edit($id)
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $data = $request->except(['_token', '_method', 'lrn']);

        if($this->examinees->updatePeriod($data)){
            alert()->success("Updating data successful", 'Success!')->persistent('Close');
        }else{
            alert()->error("Something went wrong in updating the data", 'Oops!')->persistent('Close');
        }

        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus($id)
    {
        if ($this->examinees->updateStatus($id)){
            return response()->json(['result' => 'success']);
        }
        return response()->json(['result' => 'failed']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->examinees->deleteExaminee($id))
            return response()->json(['result' => 'success']);

        return response()->json(['result' => 'failed']);
    }

    /**
     * @return mixed
     */
    public function form(){
        $pdf = PDF::loadView('forms.examinee-form');
        return $pdf->stream();
    }

    /**
     * @param Request $request
     * @return Request
     */
    public function list(Request $request){
        $data = $this->examinees->reportData($request);
        if(empty($data['error'])){
            $pdf = PDF::loadView('forms.examinee-list', $data);
            return $pdf->stream();
        }else{
            alert()->error('Missing data from ' . implode (", ", $data['error']). '.', 'Oops!')->persistent('Close');
            return back();
        }

    }
}
