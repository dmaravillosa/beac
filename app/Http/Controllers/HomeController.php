<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->first_login){
            if(Auth::user()->hasRole(config('constants.DEPED_ADMIN')))
                return view('pages.admin_home');
            else if(Auth::user()->hasRole(config('constants.DIVISION_SUPERVISOR')))
                return view('pages.supervisor_home');
            else
                return view('pages.staff_home');
        }else{
            return view('pages.users.reset');
        }

    }
}
