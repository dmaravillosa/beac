<?php

namespace App\Http\Controllers;

use App\Repositories\PeriodRepository;
use Illuminate\Http\Request;

class PeriodsController extends Controller
{
    /**
     * View directory
     * @var string
     */
    protected $view = 'pages.admin.periods';

    /**
     * @var PeriodRepository Repository
     */
    protected $periods;

    /**
     * PeriodsController constructor.
     * @param PeriodRepository $periodRepository
     * @param Request $request
     */
    function __construct(PeriodRepository $periodRepository, Request $request)
    {
        $this->middleware(['role:DEPED_ADMIN']);
        $this->periods = $periodRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->view);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function data(){
        return $this->periods->getAllData();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token', '_method', 'id']);

        if($this->periods->addPeriod($data)){
            alert()->success("Adding data successful", 'Success!')->persistent('Close');
        }else{
            alert()->error("Something went wrong in adding the data", 'Oops!')->persistent('Close');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->except(['_token', '_method']);

        if($this->periods->updatePeriod($data)){
            alert()->success("Updating data successful", 'Success!')->persistent('Close');
        }else{
            alert()->error("Something went wrong in updating the data", 'Oops!')->persistent('Close');
        }

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus($id)
    {
        if ($this->periods->updateStatus($id)){
            return response()->json(['result' => 'success']);
        }
        return response()->json(['result' => 'failed']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->periods->deletePeriod($id))
            return response()->json(['result' => 'success']);

        return response()->json(['result' => 'failed']);
    }
}
