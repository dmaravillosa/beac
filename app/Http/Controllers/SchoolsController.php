<?php

namespace App\Http\Controllers;

use App\Http\Requests\SchoolRequest;
use App\Repositories\SchoolRepository;
use App\School;
use Illuminate\Http\Request;

class SchoolsController extends Controller
{
    /**
     * View directory
     * @var string
     */
    protected $view = 'pages.admin.schools';

    /**
     * @var SchoolRepository Repository
     */
    protected $schools;

    /**
     * SchoolsController constructor.
     * @param SchoolRepository $schoolRepository
     * @param Request $request
     */
    function __construct(SchoolRepository $schoolRepository, Request $request)
    {
        $this->middleware(['role:DEPED_ADMIN']);
        $this->schools = $schoolRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->view);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function data()
    {
        return $this->schools->getAllData();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SchoolRequest $request)
    {
        $data = $request->except(['_token', '_method', 'id']);

        if($this->schools->addSchool($data)){
            alert()->success("Adding data successful", 'Success!')->persistent('Close');
        }else{
            alert()->error("Something went wrong in adding the data", 'Oops!')->persistent('Close');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->except(['_token', '_method']);

        if($this->schools->updateSchool($data)){
            alert()->success("Data update successful", 'Success!')->persistent('Close');
        }else{
            alert()->error("Something went wrong in updating the data", 'Oops!')->persistent('Close');
        }

        return redirect()->back();
    }

    public function updateStatus($id)
    {
        if ($this->schools->updateStatus($id)){
            return response()->json(['result' => 'success']);
        }
        return response()->json(['result' => 'failed']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->schools->deleteSchool($id))
            return response()->json(['result' => 'success']);

        return response()->json(['result' => 'failed']);
    }
}
