<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    /**
     * View directory
     * @var string
     */
    protected $view = 'pages.admin.users';

    /**
     * @var UserRepository Repository
     */
    protected $users;

    /**
     * UsersController constructor.
     * @param UserRepository $userRepository
     * @param Request $request
     */
    function __construct(UserRepository $userRepository, Request $request)
    {
        $this->middleware(['role:DEPED_ADMIN']);
        $this->users = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->view);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function data()
    {
        return $this->users->getAllData();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->except(['_token', '_method']);

        if($this->users->updateUser($data)){
            alert()->success("Updating profile successful", 'Success!')->persistent('Close');
        }else{
            alert()->error("Something went wrong in updating the profile", 'Oops!')->persistent('Close');
        }

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus($id)
    {
        if ($this->users->updateStatus($id)){
            return response()->json(['result' => 'success']);
        }
        return response()->json(['result' => 'failed']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->users->deleteUser($id))
            return response()->json(['result' => 'success']);

        return response()->json(['result' => 'failed']);
    }
}
