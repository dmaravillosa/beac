<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SchoolRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'head' => 'required',
            'designation' => 'required',
            'address' => 'required',
            'school_type' => 'required',
            'classification' => 'required|in:Public,Private',
            'division_id' => 'required|exists:pgsql.organizations.division,id',
            'cluster_id' => 'required|exists:pgsql.organizations.cluster,id'
        ];
    }
}
