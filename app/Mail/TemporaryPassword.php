<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TemporaryPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var user
     */
    public $user;

    /**
     * @var temp_password
     */
    protected $temp_password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $temp_password)
    {
        $this->user = $user;
        $this->temp_password = $temp_password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.temp_password')
            ->with([
                'temp_password' => $this->temp_password
            ]);
    }
}
