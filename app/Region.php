<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    /**
     * @var string
     */
    protected $table = 'organizations.region';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cluster(){
        return $this->hasMany(Cluster::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function division(){
        return $this->hasMany(Division::class);
    }
}
