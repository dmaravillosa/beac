<?php

namespace App\Repositories;

use App\Center;
use App\Repositories\Eloquent\Repository;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class CenterRepository
 * @package App\Repositories
 */
class CenterRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'App\Center';
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getAllData(){
        $centers = Center::with('cluster');

        return DataTables::of($centers)
            ->editColumn('cluster', function(Center $center){
                return $center->cluster()->pluck('cluster')[0];
            })
            ->editColumn('cluster_code', function(Center $center){
                return $center->cluster()->pluck('cluster_code')[0];
            })
            ->editColumn('status', function(Center $center) {
                return $center->status == 'enabled' ? '<h5><span class="badge badge-primary">Enabled</span></h5>' : '<h5><span class="badge badge-danger">Disabled</span></h5>';
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    public function addCenter($data){
        $center = Center::create($data);
        return $center;
    }

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function updateCenter($data){
        $update = Center::where('id', $data['id'])->update($data);
        return $update;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function updateStatus($id){
        $center = Center::findOrFail($id);
        $status = $center->status == 'enabled' ? 'disabled' : 'enabled';

        $update = Center::where('id', $id)->update(['status' => $status]);
        return $update;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteCenter($id){
        $center = Center::findOrFail($id);

        $delete = $center->destroy($id);
        return $delete;
    }

}