<?php
/**
 * Created by PhpStorm.
 * User: danielmaravillosa
 * Date: 29/11/2018
 * Time: 3:50 PM
 */

namespace App\Repositories;

use App\Center;
use App\Cluster;
use App\Examinee;
use App\Region;
use App\Repositories\Eloquent\Repository;
use App\School;
use Yajra\DataTables\Facades\DataTables;

class ExamineeRepository extends Repository
{

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'App\Examinee';
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getAllData(){
        $examinee = Examinee::all();

        return DataTables::of($examinee)
            ->editColumn('name', function(Examinee $examinee) {
                return $examinee->last_name . ", " . $examinee->first_name . " " . $examinee->middle_name ;
            })
            ->editColumn('school', function(Examinee $examinee) {
                return $examinee->school()->first()->name;
            })
            ->editColumn('status', function(Examinee $examinee) {
                return $examinee->status == 'enabled' ? '<h5><span class="badge badge-primary">Enabled</span></h5>' : '<h5><span class="badge badge-danger">Disabled</span></h5>';
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function addPeriod($data){
        $period = Examinee::create($data);
        return $period;
    }

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function updatePeriod($data){
        $update = Examinee::where('id', $data['id'])->update($data);
        return $update;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function updateStatus($id){
        $examinee = Examinee::findOrFail($id);
        $status = $examinee->status == 'enabled' ? 'disabled' : 'enabled';

        $update = Examinee::where('id', $id)->update(['status' => $status]);
        return $update;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteExaminee($id){
        $examinee = Examinee::findOrFail($id);

        $delete = $examinee->destroy($id);
        return $delete;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function reportData($data){
        //return values
        $request = array();
        $request['error'] = array();

        //fetch all data for pdf report
        $cluster = Cluster::where('id', $data['cluster_id'])->first();

        $request['cluster'] = $cluster->cluster;
        $request['code'] = $cluster->cluster_code;
        $request['region'] = $cluster->region()->first()->name;

        //check if there are registered schools in the cluster
        if($cluster->school()->get()->count()){
            $schools = $cluster->school()->get()->pluck('id');
            $examinees = Examinee::whereIn('school_id', $schools)->get();
            $request['examinees'] = $examinees;
        }else{
            array_push($request['error'],'schools');
        }

        //check if there is an assesment center registered in the cluster
        if($cluster->center()->first() != null)
            $request['center'] = $cluster->center()->first()->assessment_center;
        else {
            array_push($request['error'],'assessment center');
        }

        return $request;
    }

}