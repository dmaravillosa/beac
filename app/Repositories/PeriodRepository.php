<?php

namespace App\Repositories;

use App\Period;
use App\Repositories\Eloquent\Repository;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class PeriodRepository
 * @package App\Repositories
 */
class PeriodRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'App\Period';
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getAllData(){
        $period = Period::all();

        return DataTables::of($period)
            ->editColumn('status', function(Period $period) {
                return $period->status == 'enabled' ? '<h5><span class="badge badge-primary">Enabled</span></h5>' : '<h5><span class="badge badge-danger">Disabled</span></h5>';
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    public function addPeriod($data){
        $period = Period::create($data);
        return $period;
    }

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function updatePeriod($data){
        $update = Period::where('id', $data['id'])->update($data);
        return $update;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function updateStatus($id){
        $period = Period::findOrFail($id);
        $status = $period->status == 'enabled' ? 'disabled' : 'enabled';

        $update = Period::where('id', $id)->update(['status' => $status]);
        return $update;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deletePeriod($id){
        $period = Period::findOrFail($id);

        $delete = $period->destroy($id);
        return $delete;
    }

}