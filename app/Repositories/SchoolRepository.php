<?php

namespace App\Repositories;

use App\School;
use App\Repositories\Eloquent\Repository;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class SchoolRepository
 * @package App\Repositories
 */
class SchoolRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'App\Center';
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getAllData(){
        $schools = School::with(['cluster', 'division']);

        return DataTables::of($schools)
            ->editColumn('division', function(School $school){
                return $school->division()->pluck('division')[0];
            })
            ->editColumn('cluster_code', function(School $school){
                return $school->cluster()->pluck('cluster_code')[0];
            })
            ->editColumn('status', function(School $school) {
                return $school->status == 'enabled' ? '<h5><span class="badge badge-primary">Enabled</span></h5>' : '<h5><span class="badge badge-danger">Disabled</span></h5>';
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function addSchool($data){
        $data["school_type"] = school_type($data['school_type']);
        $school = School::create($data);
        return $school;
}

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function updateSchool($data){
        if(isset($data["school_type"]))
            $data["school_type"] = school_type($data['school_type']);

        $update = School::where('id', $data['id'])->update($data);
        return $update;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function updateStatus($id){
        $school = School::findOrFail($id);
        $status = $school->status == 'enabled' ? 'disabled' : 'enabled';

        $update = School::where('id', $id)->update(['status' => $status]);
        return $update;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteSchool($id){
        $school = School::findOrFail($id);

        $delete = $school->destroy($id);
        return $delete;
    }

}