<?php

namespace App\Repositories;

use App\User;
use App\Repositories\Eloquent\Repository;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'App\User';
    }

    public function getAllData(){
        $users = User::all();

        return DataTables::of($users)
            ->editColumn('status', function(User $user) {
                return $user->status == 'enabled' ? '<h5><span class="badge badge-primary">Enabled</span></h5>' : '<h5><span class="badge badge-danger">Disabled</span></h5>';
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function updateUser($data){
        $update = User::where('id', $data['id'])->update($data);
        return $update;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function updateStatus($id){
        $user = User::findOrFail($id);
        $status = $user->status == 'enabled' ? 'disabled' : 'enabled';

        $update = User::where('id', $id)->update(['status' => $status]);
        return $update;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteUser($id){
        $user = User::findOrFail($id);

        $delete = $user->destroy($id);
        return $delete;
    }

}