<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolType extends Model
{
    /**
     * @var string
     */
    protected $table = 'organizations.school_type';
}
