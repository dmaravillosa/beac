<?php

namespace App\Traits;

/**
 * Trait Encryptable
 */
trait Encryptable
{

    /**
     * Manipulate the attributes on get
     *
     * @param $key
     * @return bool|string
     */
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);

        if (in_array($key, $this->encryptable))
            $value = custom_crypt($value, 'd');

        return $value;
    }


    /**
     * Model set attributes on field
     * @param $key
     * @param $value
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->encryptable) && strlen($value) < 82)
            $value = custom_crypt($value, 'e');


        return parent::setAttribute($key, $value);
    }
}