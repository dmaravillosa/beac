<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
//use App\Traits\Encryptable;

class User extends Authenticatable
{
//    use Encryptable;
    use Notifiable;
    use HasRoles;

    /**
     * @var array
     */
//    protected $encryptable = [
//        'last_name',
//        'first_name',
//        'middle_name',
//        'designation',
//    ];

    protected $table = 'users.user';

    protected $guard_name = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'middle_name', 'last_name', 'email', 'password', 'designation', 'school_type', 'classification', 'region_access', 'division_access', 'created_by', 'first_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];
}
