<?php
/**
 * Created by PhpStorm.
 * User: danielmaravillosa
 * Date: 23/10/2018
 * Time: 5:26 PM
 */

return [

    /**
     * ACCESS ROLES
     */
    'DEPED_ADMIN' => 'DEPED_ADMIN',
    'DIVISION_SUPERVISOR' => 'DIVISION_SUPERVISOR',
    'SERVICE_PROVIDER_STAFF' => 'SERVICE_PROVIDER_STAFF',


    /**
     * For displaying of image
     */
    'profile_image' => env('APP_URL') . '/storage/profile/',

    'logos' => env('APP_URL') . '/storage/form_logos',

    'reg-form' => '/storage/forms/registration-form.pdf',
];