<?php

use Faker\Generator as Faker;

$factory->define(\App\Examinee::class, function (Faker $faker) {
    return [
        'lrn' => $faker->unique()->numberBetween(120000, 129999),
        'first_name' => $faker->firstName,
        'middle_name' => $faker->lastName,
        'last_name' => $faker->lastName,
        'birthdate' => $faker->date(),
        'gender' => "Male",
        'school_id' => \App\School::all()->random()->id,
    ];
});
