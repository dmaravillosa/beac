<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {

    $division = \App\Division::all()->random();


    return [
        'email' => $faker->unique()->safeEmail,
        'first_name' => $faker->firstName,
        'middle_name' => $faker->lastName,
        'last_name' => $faker->lastName,

        'designation' => $faker->jobTitle,
        'school_type' => \App\SchoolType::all()->random()->name,
        'classification' => 'Public',
        'region_access' => \App\Region::where('id', $division->region_id)->first()->code,
        'division_access' => $division->division,
        'created_by' => 'Created by: Seeder',
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});
