<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users.user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');

            $table->string('designation');
            $table->string('school_type');
            $table->enum('classification', ['Public', 'Private'])->comment('Value: Public/Private');
            $table->string('region_access');
            $table->string('division_access');
            $table->string('created_by')->comment('Created by: *user');
            $table->string('first_login')->default(1)->comment('User first login changed temporary password');

            $table->enum('status', ['enabled', 'disabled'])->default('enabled')->comment('Value: enabled/disabled');
            $table->string('image')->default('default_profile.jpg');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users.user');
    }
}
