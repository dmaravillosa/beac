<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessmentCenterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations.center', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cluster_id')->unsigned();
            $table->string('assessment_center');
            $table->enum('status', ['enabled', 'disabled'])->default('enabled')->comment('Value: enabled/disabled');
            $table->timestamps();

            $table->foreign('cluster_id')
                ->references('id')
                ->on('organizations.cluster')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations.center');
    }
}
