<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations.school', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('head');
            $table->string('designation');
            $table->string('address');
            $table->string('school_type');
            $table->enum('classification', ['Public', 'Private'])->comment('Value: Public/Private');
            $table->integer('division_id')->unsigned();
            $table->integer('cluster_id')->unsigned();
            $table->enum('status', ['enabled', 'disabled'])->default('enabled')->comment('Value: enabled/disabled');
            $table->timestamps();

            $table->foreign('division_id')
                ->references('id')
                ->on('organizations.division')
                ->onDelete('cascade');

            $table->foreign('cluster_id')
                ->references('id')
                ->on('organizations.cluster')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations.school');
    }
}
