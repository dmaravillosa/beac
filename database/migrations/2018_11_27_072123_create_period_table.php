<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records.period', function (Blueprint $table) {
            $table->increments('id');
            $table->date('from')->comment("Registration date start");
            $table->date('to')->comment("Registration date end");
            $table->enum('status', ['enabled', 'disabled'])->default('enabled')->comment('Value: enabled/disabled');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('period');
    }
}
