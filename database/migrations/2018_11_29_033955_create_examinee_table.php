<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamineeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records.examinee', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lrn')->comment('Learners Reference Number');
            $table->string('last_name');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('birthdate');
            $table->enum('gender', ['Male', 'Female']);
            $table->string('image')->default('default_examinee.jpg');
            $table->integer('school_id')->unsigned();
            $table->enum('status', ['enabled', 'disabled'])->default('enabled')->comment('Value: enabled/disabled');
            $table->timestamps();

            $table->foreign('school_id')
                ->references('id')
                ->on('organizations.school')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records.examinee');
    }
}
