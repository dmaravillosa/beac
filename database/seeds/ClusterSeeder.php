<?php

use Illuminate\Database\Seeder;

class ClusterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Cluster::create(['region_id' => 6, 'cluster_code' => 'C', 'cluster' => 'Cavite']);
        \App\Cluster::create(['region_id' => 6, 'cluster_code' => 'L', 'cluster' => 'Laguna']);
        \App\Cluster::create(['region_id' => 6, 'cluster_code' => 'B', 'cluster' => 'Batangas']);
        \App\Cluster::create(['region_id' => 6, 'cluster_code' => 'R', 'cluster' => 'Rizal']);
        \App\Cluster::create(['region_id' => 6, 'cluster_code' => 'Q', 'cluster' => 'Quezon']);

        \App\Center::create(['cluster_id' => 4, 'assessment_center' => 'Rizal National HighSchool']);
    }
}
