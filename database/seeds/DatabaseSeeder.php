<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Static Data (Include this when seeding in live)
        $this->call(RoleSeeder::class);
        $this->call(RegionSeeder::class);
        $this->call(DivisionSeeder::class);
        $this->call(ClusterSeeder::class);
        $this->call(SchoolTypeSeeder::class);

        //Test Seeds (Comment when seeding in live)
        $this->call(UsersTableSeeder::class);
        $this->call(SchoolTableSeeder::class);
        $this->call(RegPeriodSeeder::class);
        $this->call(ExamineeSeeder::class);
    }
}
