<?php

use Illuminate\Database\Seeder;

class DivisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Division::create(['region_id' => '6', 'division' => 'Batangas']);
        \App\Division::create(['region_id' => '6', 'division' => 'Cavite']);
        \App\Division::create(['region_id' => '6', 'division' => 'Laguna']);
        \App\Division::create(['region_id' => '6', 'division' => 'Quezon']);
        \App\Division::create(['region_id' => '6', 'division' => 'Rizal']);
        \App\Division::create(['region_id' => '6', 'division' => 'Antipolo City']);
        \App\Division::create(['region_id' => '6', 'division' => 'Bacoor City']);
        \App\Division::create(['region_id' => '6', 'division' => 'Batangas City']);
        \App\Division::create(['region_id' => '6', 'division' => 'Biñan City']);
        \App\Division::create(['region_id' => '6', 'division' => 'Cabuyao City']);
        \App\Division::create(['region_id' => '6', 'division' => 'Calamba City']);
        \App\Division::create(['region_id' => '6', 'division' => 'Cavite City']);
        \App\Division::create(['region_id' => '6', 'division' => 'Dasmariñas City']);
        \App\Division::create(['region_id' => '6', 'division' => 'Lipa City']);
        \App\Division::create(['region_id' => '6', 'division' => 'Lucena City']);
        \App\Division::create(['region_id' => '6', 'division' => 'San Pablo City']);
        \App\Division::create(['region_id' => '6', 'division' => 'Sta. Rosa City']);
        \App\Division::create(['region_id' => '6', 'division' => 'Tanauan City']);
        \App\Division::create(['region_id' => '6', 'division' => 'Tayabas City']);
        \App\Division::create(['region_id' => '6', 'division' => 'General Trias City']);
    }
}
