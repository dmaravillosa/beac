<?php

use Illuminate\Database\Seeder;

class ExamineeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Examinee::create([
            'lrn' => '121456',
            'last_name' => 'Doe',
            'first_name' => 'Jason',
            'middle_name' => 'Art',
            'birthdate' => '2007-11-29',
            'gender' => 'Male',
            'school_id' => 1,
        ]);

        factory(\App\Examinee::class, 60)->create();
    }
}
