<?php

use Illuminate\Database\Seeder;

class RegPeriodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Period::create([
            'from' => \Carbon\Carbon::now(),
            'to' => \Carbon\Carbon::now()->addDays(10),
        ]);
    }
}
