<?php

use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Region::create(['code' => 'NCR', 'name' => 'National Capital Region']);
        \App\Region::create(['code' => 'Region I', 'name' => 'Ilocos Region']);
        \App\Region::create(['code' => 'CAR', 'name' => 'Cordillera Administrative Region']);
        \App\Region::create(['code' => 'Region II', 'name' => 'Cagayan Valley']);
        \App\Region::create(['code' => 'Region III', 'name' => 'Central Luzon']);
        \App\Region::create(['code' => 'Region 4A', 'name' => 'CALABARZON']);
        \App\Region::create(['code' => 'MIMAROPA', 'name' => 'Southwestern Tagalog Region']);
        \App\Region::create(['code' => 'Region V', 'name' => 'Bicol Region']);
        \App\Region::create(['code' => 'Region VI', 'name' => 'Western Visayas']);
        \App\Region::create(['code' => 'Region VII', 'name' => 'Central Visayas']);
        \App\Region::create(['code' => 'Region VIII', 'name' => 'Eastern Visayas']);
        \App\Region::create(['code' => 'Region IX', 'name' => 'Zamboanga Peninsula']);
        \App\Region::create(['code' => 'Region X', 'name' => 'Northern Mindanao']);
        \App\Region::create(['code' => 'Region XI', 'name' => 'Davao Region']);
        \App\Region::create(['code' => 'Region XII', 'name' => 'SOCCSKSARGEN']);
        \App\Region::create(['code' => 'Region XIII', 'name' => 'Caraga Region']);
        \App\Region::create(['code' => 'ARMM', 'name' => 'Autonomous Region in Muslim Mindanao']);
    }
}
