<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Role::create(['name' => config('constants.DEPED_ADMIN')]);
        \Spatie\Permission\Models\Role::create(['name' => config('constants.DIVISION_SUPERVISOR')]);
        \Spatie\Permission\Models\Role::create(['name' => config('constants.SERVICE_PROVIDER_STAFF')]);
    }
}
