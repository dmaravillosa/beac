<?php

use Illuminate\Database\Seeder;

class SchoolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\School::create([
            'name' => 'Rizal National High School',
            'head' => 'Ms. Jane Doe',
            'designation' => 'Test',
            'address' => 'Angono, Rizal',
            'school_type' => 'Kinder',
            'classification' => 'Public',
            'division_id' => '5',
            'cluster_id' => '4',
        ]);
    }
}
