<?php

use Illuminate\Database\Seeder;

class SchoolTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\SchoolType::create(['name' => 'Kinder']);
        \App\SchoolType::create(['name' => 'Grade 1-6']);
        \App\SchoolType::create(['name' => 'Grade 7-10']);
        \App\SchoolType::create(['name' => 'Grade 11-12']);
    }
}
