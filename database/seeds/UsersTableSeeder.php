<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class)->create([
            'email' => 'johndoe@gmail.com',
            'password' => bcrypt('secret'),
            'first_login' => '0'
        ])->assignRole(config('constants.DEPED_ADMIN'));

        factory(\App\User::class)->create([
            'email' => 'janedoe@gmail.com',
            'password' => bcrypt('secret'),
            'first_login' => '0'
        ])->assignRole(config('constants.DIVISION_SUPERVISOR'));

        factory(\App\User::class)->create([
            'email' => 'jackdoe@gmail.com',
            'password' => bcrypt('secret'),
            'first_login' => '0'
        ])->assignRole(config('constants.SERVICE_PROVIDER_STAFF'));
    }
}
