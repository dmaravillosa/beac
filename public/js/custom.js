/**
 * Preview the image in select file
 * @param input - the input file element
 * @param img_id - the image id to be display the preview file
 * @constructor
 */
function readURL(input, img_id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(img_id).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

/**
 * Password variations for strengthen the password
 * @param pass - password
 * @returns digits: boolean, lowercase: boolean, uppercase: boolean, symbols: boolean
 */
function passwordVariations(pass) {
    return {
        digits: /\d/.test(pass),
        lowercase: /[a-z]/.test(pass),
        uppercase: /[A-Z]/.test(pass),
        symbols: /\W/.test(pass)
    };

}

/**
 * Display tooltip bar for password strength
 * @param ele
 */
function passwordStrengthTip(ele) {
    var pass = ele.val();
    var variations = passwordVariations(pass);
    var password =  $('#password');

    var password_strength = "";
    for (var check in variations) {
        if (!variations[check]) {
            password_strength += 'Must contain atleast ' + check + "<br>";
        }
    }

    if (password_strength === "") {
       password.tooltip('destroy');
    } else {
        if (password.next('div.tooltip:visible').length) {
            password.attr('title', password_strength).tooltip('fixTitle').tooltip('show');
        } else {
            password.tooltip({
                'trigger': 'hover click focus',
                'title': password_strength,
                'html': true,
                'placement': 'right'
            });
            password.tooltip('show');
        }
    }
}

/**
 * Evalutes the password score
 * @param pass - password
 * @returns {*}
 */
function scorePassword(pass) {
    var score = 0;

    if (!pass)
        return score;

    // award every unique letter until 5 repetitions
    var letters = new Object();
    for (var i = 0; i < pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }
    // bonus points for mixing it up
    var variations = passwordVariations(pass);

    variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] === true) ? 1 : 0;
    }

    score += (variationCount - 1) * 10;

    return parseInt(score);
}

/**
 * Check password strength
 * @param pass - password
 * @returns {*}
 */
function checkPassStrength(pass) {
    var score = scorePassword(pass);
    if (score > 80)
        return "strong";
    if (score > 60)
        return "good";
    if (score >= 30)
        return "weak";

    return "";
}

/**
 * Format Javascript Date to F-d-Y
 *
 * @param date
 * @returns {string}
 */
function formatDateToReadable(date) {
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

    var formattedDate = new Date(date);
    var d = formattedDate.getDate();
    var m =  formattedDate.getMonth();
    var y = formattedDate.getFullYear();

    return months[m] + ' ' + d + ', ' + y;
}

/**
 * Format JavaScript Date to yyyy-mm-dd
 *
 * @param date - date string to be convert
 * @returns {string}
 */
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

/**
 * Clear the form input fields
 */
function clearForm() {
    $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
    $('img').attr('src', '');
    $(':checkbox, :radio').prop('checked', false);
    clearError();
}

/**
 * Clear Form error
 **/
function clearError() {
    $('.invalid-feedback').remove();
    $('.form-control').removeClass('is-invalid');
}

/**
 * Form error messageformat
 *
 * @param message
 * @returns {string}
 */
function errorMessage(message) {
    return '<span class="help-block"><strong>' + message + '</strong>'
}

/**
 * Objectify the form data
 * @param formData
 * @returns Object
 */
function convertFormDataToObject(formData) {
    var data = {};
    for (var pair of formData.entries()) data[pair[0]] = pair[1];
    return data;
}

/**
 * Wildcard String match
 *
 * @link https://stackoverflow.com/questions/26246601/wildcard-string-comparison-in-javascript
 * @param str
 * @param rule
 * @returns {boolean}
 */
function matchRule(str, rule) {
    // "."  => Find a single character, except newline or line terminator
    // ".*" => Matches any string that contains zero or more characters
    rule = rule.split("*").join(".*");

    // "^"  => Matches any string with the following at the beginning of it
    // "$"  => Matches any string with that in front at the end of it
    rule = "^" + rule + "$";

    //Create a regular expression object for matching string
    var regex = new RegExp(rule);

    //Returns true if it finds a match, otherwise it returns false
    return regex.test(str);
}

function formatReadableDate(){

}