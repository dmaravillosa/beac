@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create User</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        {{--Role--}}
                        <div class="form-group row">
                            <label for="role" class="col-md-4 col-form-label text-md-right">Role</label>

                            <div class="col-md-6">
                                {{ Form::select('role', \Spatie\Permission\Models\Role::all()->pluck('name'), null , ['placeholder' => 'Assign Role..', 'class' => 'form-control' . ($errors->has('role') ? ' is-invalid' : '')]) }}
                                @if ($errors->has('role'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{--Role--}}


                        {{--first name--}}
                        <div class="form-group row">
                            <label for="first_name" class="col-md-4 col-form-label text-md-right">First Name</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required>

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{--first name--}}

                        {{--middle name--}}
                        <div class="form-group row">
                            <label for="middle_name" class="col-md-4 col-form-label text-md-right">Middle Name</label>

                            <div class="col-md-6">
                                <input id="middle_name" type="text" class="form-control{{ $errors->has('middle_name') ? ' is-invalid' : '' }}" name="middle_name" value="{{ old('middle_name') }}" required>

                                @if ($errors->has('middle_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{--middle name--}}

                        {{--last name--}}
                        <div class="form-group row">
                            <label for="last_name" class="col-md-4 col-form-label text-md-right">Last Name</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required >

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{--last name--}}


                        {{--email--}}
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{--email--}}

                        {{--designation--}}
                        <div class="form-group row">
                            <label for="designation" class="col-md-4 col-form-label text-md-right">Designation</label>

                            <div class="col-md-6">
                                <input id="designation" type="text" class="form-control{{ $errors->has('designation') ? ' is-invalid' : '' }}" name="designation" value="{{ old('designation') }}" required>

                                @if ($errors->has('designation'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('designation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{--designation--}}

                        {{--School Type--}}
                        <div class="form-group row">
                            <label for="school_type" class="col-md-4 col-form-label text-md-right">School Type</label>

                            <div class="col-md-6">
                                <select class="form-control {{ $errors->has('school_type') ? ' is-invalid' : '' }}" id="school_type" name="school_type[]" multiple="multiple">
                                    @foreach(\App\SchoolType::select('id','name')->get() as $school_type)
                                        <option value="{{ $school_type->id }}">{{ $school_type->name }}</option>
                                    @endforeach
                                </select>@if ($errors->has('school_type'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('school_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{--School Type--}}

                        {{-- Classification --}}
                        <div class="form-group row">
                            <label for="role" class="col-md-4 col-form-label text-md-right">Classification</label>

                            <div class="col-md-6">
                                {{ Form::select('classification', ['public' => 'Public', 'private' => 'Private'], [ 'class' => 'form-control' . ($errors->has('classification') ? ' is-invalid' : '')]) }}
                                @if ($errors->has('classification'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('classification') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- Classification --}}

                        {{-- Region Access --}}
                        <div class="form-group row">
                            <label for="region_access" class="col-md-4 col-form-label text-md-right">Region Access</label>

                            <div class="col-md-6">
                                {{ Form::select('region_access', \App\Region::all()->pluck('code'), null, ['placeholder'=>'Pick a region..', 'class' => 'form-control' . ($errors->has('region_access') ? ' is-invalid' : '')]) }}
                                @if ($errors->has('region_access'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('region_access') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- Region Access --}}

                        {{-- Division Access --}}
                        <div class="form-group row">
                            <label for="division_access" class="col-md-4 col-form-label text-md-right">Division Access</label>

                            <div class="col-md-6">
                                <input id="division_access" type="text" class="form-control{{ $errors->has('division_access') ? ' is-invalid' : '' }}" name="division_access" value="{{ old('division_access') }}" required>
                                @if ($errors->has('division_access'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('division_access') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- Division Access --}}

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('select').selectpicker();
</script>
@endsection


