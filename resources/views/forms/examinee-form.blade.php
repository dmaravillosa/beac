@extends('forms.layout')

@section('content')
<h2 class="text-center sans" style="font-size: 18px">Registration Form</h2>

    {{--first row--}}
    <table width="100%" style="font-size: 14px;">
        <tr>
            <td width="70%">
                <div style="border: 1px solid black; margin: 2px 10px 5px 0px;  padding: 3px;">
                    <table width="100%">
                        <tr>
                            <td width="50%">Cluster: </td>
                            <td>Division: </td>
                        </tr>
                        <tr>
                            <td>District: </td>
                            <td>Testing Venue: </td>
                        </tr>
                        <tr>
                            <td>Testing Date: </td>
                            <td>BEAC Examinee Number: </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td rowspan="2" width="30%">
                <div class="text-center" style="height: auto; border: 1px solid black; margin-top: 10px; height: 190px">
                    <b>Staple the picture here.<br>
                    <span>(2x2)</span></b>
                </div>
            </td>
        </tr>
        <tr>
            <td width="70%">
                <div style="height:auto; border: 1px solid black; margin: 5px 10px 2px 0px; padding: 3px;">
                    To the Student:<br>
                    1. Print all the information clearly and legibly.<br>
                    2. Do not leave a blank box.<br>
                    3. Staple a recent 2x2 picture in the box on the right. Be sure to print your name at the back of the photo.<br>
                </div>
            </td>
        </tr>
    </table>

    {{--second row--}}
    <div style="border: 1px solid black">
        <table>
            <tr>
                <td>
                    <span style="font-size: 14px"><b>Learner's Reference Number (LRN)</b></span>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <span style="font-size: 14px"><b>Last Name</b></span>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <span style="font-size: 14px"><b>First Name</b></span>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <span style="font-size: 14px"><b>Middle Name</b></span>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <span style="font-size: 14px"><b>Division</b></span>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <span style="font-size: 14px"><b>Name of School</b></span>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <span style="font-size: 14px"><b>School Address</b></span>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <span style="font-size: 14px"><b>Date of Birth</b></span>
                </td>
                <td>

                </td>
            </tr>
        </table>

    </div>
@endsection
