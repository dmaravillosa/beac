<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <style>
        body{
            line-height: 130%;
        }

        .sans {
            font-family: Arial,Helvetica Neue,Helvetica,sans-serif;
        }

        .formal-text
        {
            font-family: "Times New Roman", Times, serif;
        }

        .logo{
            width: 100px;
            height: 100px;
        }
    </style>

    <style>
        @page { margin: 100px 25px; }
        header { position: fixed; top: -60px; left: 0px; right: 0px; height: 150px; }
    </style>

</head>
<body>
<header>
    {{--REUSABLE TITLE BLOCK--}}
    <p class="text-center" style=" margin-top: 30px;">
        <b>{{ date('Y') }} BASIC EDUCATION ASSESSMENT IN {{ $region }} (BEAC)<br>
            OFFICIAL LIST OF EXAMINEES
        </b>
    </p>
    <p class="text-center">
        <b>
            {{ strtoupper($cluster) }} CLUSTER
        </b>
    </p>

    <div style="margin-left: 20px">
        <span><b>Assessment Center: {{ $center }}</b></span><br>
        <span><b>Room No: ___________________</b></span>
    </div>
</header>

<div style="margin: 110px 0px 0px 20px">
    <table width="80%">
        <tr>
            <th><b>BRN No.</b></th>
            <th><b>Name of Examinees</b></th>
        </tr>
        @foreach($examinees as $indexKey => $examinee)
            @if(($indexKey + 1) % 31 == 0)
                <tr>
                    <td height="110px"></td>
                </tr>
                <tr>
                    <th><b>BRN No. </b></th>
                    <th><b>Name of Examinees</b></th>
                </tr>
            @endif
        <tr>
            <td>
                {{ $examinee['lrn'] }}
            </td>
            <td>
                {{ $examinee['last_name'] . ', ' . $examinee['first_name'] . ' ' . $examinee['middle_name'] }}
            </td>
        </tr>
        @endforeach
    </table>
</div>

</body>
</html>