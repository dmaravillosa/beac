<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <style>
        body{
            line-height: 130%;
        }

        .sans {
            font-family: Arial,Helvetica Neue,Helvetica,sans-serif;
        }

        .formal-text
        {
            font-family: "Times New Roman", Times, serif;
        }

        .logo{
            width: 100px;
            height: 100px;
        }
    </style>

</head>

<body>
<table width="100%" class="text-center">
    <tr>
        <td><img class="logo" src="{{ env('APP_URL') . '/storage/form_logos/depedlogo.jpg'  }}"></td>
        <td>
            <span class="formal-text">Republic of the Philippines</span><br>
            <span class="formal-text" style="font-size: 20px">Department of Education</span><br>
            @if(isset($request))
                <span class="formal-text" style="color: red"><b>REGION IV-A CALABARZON</b></span><br>
                <span class="sans" style="font-size: 15px"> Gate 2 Karangalan Village<br>
                 1900 Cainta, Rizal </span><br>
            @endif
        </td>
        <td><img class="logo" src="{{ env('APP_URL') . '/storage/form_logos/calabarzon.jpeg'  }}"></td>
    </tr>
</table>
<br><br>
<h5 class="text-center sans" style="font-size: 18px"><b>BASIC EDUCATION ASSESSMENT in CALABARZON (BEAC)</b></h5>
@yield('content')
</body>
</html>