@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            {{ csrf_field() }}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Manage Assessment Centers</div>

                    <div class="card-body">
                        <a class="btn btn-info mb-3" id="addCenter" data-toggle="modal" data-target="#editCenter">Create Assessment Center</a>
                        <table class="table table-bordered" id="centers-table">
                            <thead>
                            <tr>
                                <th>Assessment Center</th>
                                <th>Cluster</th>
                                <th>Cluster Code</th>
                                <th>Status</th>
                                <th width="20px">Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit User Modal -->
    <div class="modal fade" id="editCenter" tabindex="-1" role="dialog" aria-labelledby="editCenterTitle" aria-hidden="true">
        {!! Form::open(['id' => 'modal_form', 'class' => 'form-horizontal', 'route' => 'centers.edit', disableValidateIfTesting()]) !!}
        {{--{!! method_field('PUT') !!}--}}
        <input type="hidden" name="id" id="id">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editCenterTitle">Add Assessment Center</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{--assessment center--}}
                    <div class="form-group row">
                        <label for="assessment_center" class="col-md-4 col-form-label">Assessment Center</label>

                        <div class="col-md-12">
                            <input id="assessment_center" type="text" class="form-control{{ $errors->has('assessment_center') ? ' is-invalid' : '' }}" name="assessment_center" value="{{ old('assessment_center') }}" required>

                            @if ($errors->has('assessment_center'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('assessment_center') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{--assessment centere--}}

                    {{-- Cluster Id --}}
                    <div class="form-group row">
                        <label for="cluster_id" class="col-md-4 col-form-label">Cluster</label>

                        <div class="col-md-12">
                            {{--{{ Form::select('cluster_id', \App\Cluster::all()->pluck('cluster'), \App\Cluster::where('id', '3')->pluck('id')->first(), ['placeholder'=>'Pick a cluster..', 'class' => 'form-control' . ($errors->has('cluster_id') ? ' is-invalid' : '')]) }}--}}
                            <select class="form-control {{ $errors->has('cluster_id') ? ' is-invalid' : '' }}" id="cluster_id" name="cluster_id">
                                @foreach(\App\Cluster::all() as $cluster)
                                    <option value="{{ $cluster->id }}">{{ $cluster->cluster }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('cluster_id'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('cluster_id') }}</strong>
                                </span>
                            @endif
                            </div>
                    </div>
                    {{-- Cluster Id --}}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    {{  Form::submit('Submit', ['class' => 'btn btn-primary', 'id' => 'modal_submit']) }}
                </div>
            </div>
        </div>
        </form>
    </div>


    <script type="text/javascript">
        $(document).ready(function() {

            $('select').selectpicker();

            var table = $('#centers-table').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: '{{ route('centers.data') }}',
                columns: [
                    { data: 'assessment_center', name: 'assessment_center' },
                    { data: 'cluster', name: 'cluster' },
                    { data: 'cluster_code', name: 'cluster_code' },
                    { data: 'status', name: 'status' },
                    { data: null,
                        render: function ( data, type, row ) {
                            return '<button id="edit" type="button" data-toggle="modal" data-target="#editCenter" class="btn btn-secondary btn-block edit">Edit</button>' +
                                '<button id="status" type="button" class="btn btn-warning btn-block status">Enable/Disable</button>' +
                                '<button id="delete" type="button" class="btn btn-danger btn-block delete">Delete</button>';
                        }
                    }
                ]
            });

            $("#addCenter").on('click', function() {
                //Modal tag manipulations
                $("#_method").remove();
                $("#editCenterTitle").text('Add Assessment Center')

                //clearing current data values
                $("#assessment_center").val('');
                $("#cluster_id").val('');
                $(".filter-option-inner-inner").text('')
            });

            $("#centers-table tbody").on( 'click', 'button.edit', function () {
                var data = table.row( $(this).parents('tr') ).data();

                //Modal tag manipulations
                $('#modal_form').append('<input type="hidden" id="_method" name="_method" value="PUT" />');
                $("#editCenterTitle").text('Edit Assessment Center')

                //setting current data values
                $("#id").val(data['id']);
                $("#assessment_center").val(data['assessment_center']);

                //set selected on modal select cluster
                $("#cluster_id").val(data['cluster_id']);
                $(".filter-option-inner-inner").text(data['cluster'])
            });

            $("#centers-table tbody").on( 'click', 'button.status', function () {
                swal({
                    title: "Change Status",
                    text: "Are you sure you want to change the status of this data?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((status) => {
                    if (status) {
                        var data = table.row( $(this).parents('tr') ).data();
                        $.ajax({
                            type: "PUT",
                            url: 'centers/' + data['id'] + '/status',
                            data: {
                                'id' : data['id'],
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(result) {
                                swal("Data successfully updated.", {
                                    icon: "success",
                                });
                                table.ajax.reload();
                            },
                            error: function (error) {
                                swal({
                                    title: "Error",
                                    text: "Error occurred when updating data.",
                                    icon: "error",
                                });
                            }
                        });
                    }
                });
            });

            $("#centers-table tbody").on( 'click', 'button.delete', function () {
                swal({
                    title: "Delete Data",
                    text: "Are you sure you want to delete this data?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        var data = table.row( $(this).parents('tr') ).data();
                        $.ajax({
                            type: "DELETE",
                            url: 'centers/' + data['id'] + '/delete',
                            data: {
                                'id' : data['id'],
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(result) {
                                swal("Data successfully deleted.", {
                                    icon: "success",
                                });
                                table.ajax.reload();
                            },
                            error: function (error) {
                                swal({
                                    title: "Error",
                                    text: "Error occurred when deleting data.",
                                    icon: "error",
                                });
                            }
                        });
                    }
                });
            });

            $("#modal_form").submit(function(e){
                if($("#assessment_center").val() == '' || $("#cluster_id").val() == null) {
                    swal({
                        title: "Warning",
                        text: "Please fill all inputs.",
                        icon: "warning",
                    });
                    e.preventDefault();
                }

            });


        });

    </script>

@endsection
