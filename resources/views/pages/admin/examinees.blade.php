@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            {{ csrf_field() }}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Manage Examinees</div>

                    <div class="card-body">
                        <a class="btn btn-info mb-3" id="addExaminee" data-toggle="modal" data-target="#editExaminee">Add Examinee</a>
                        <a class="btn btn-info mb-3" href="{{ asset(Config::get('constants.reg-form')) }}" id="generateForm">Generate Form</a>
                        <table class="table table-bordered" id="examinees-table">
                            <thead>
                            <tr>
                                <th>LRN</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>School</th>
                                <th>Status</th>
                                <th width="20px">Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Examinee Modal -->
    <div class="modal fade" id="editExaminee" tabindex="-1" role="dialog" aria-labelledby="editExamineeTitle" aria-hidden="true">
        {!! Form::open(['id' => 'modal_form', 'class' => 'form-horizontal', 'route' => 'examinees.edit', disableValidateIfTesting()]) !!}
        {{--{!! method_field('PUT') !!}--}}
        <input type="hidden" name="id" id="id">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editExamineeTitle">Add Examinee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    {{-- lrn --}}
                    <div class="form-group row" id="lrn_div">
                        <label for="lrn" class="col-md-4 col-form-label">LRN</label>

                        <div class="col-md-12">
                            <input id="lrn" type="text" class="form-control{{ $errors->has('lrn') ? ' is-invalid' : '' }}" name="lrn" value="{{ old('lrn') }}" required>

                            @if ($errors->has('lrn'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lrn') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    {{-- lrn --}}

                    {{--first name--}}
                    <div class="form-group row">
                        <label for="first_name" class="col-md-4 col-form-label">First Name</label>

                        <div class="col-md-12">
                            <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required>

                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    {{--first name--}}

                    {{--middle name--}}
                    <div class="form-group row">
                        <label for="middle_name" class="col-md-4 col-form-label">Middle Name</label>

                        <div class="col-md-12">
                            <input id="middle_name" type="text" class="form-control{{ $errors->has('middle_name') ? ' is-invalid' : '' }}" name="middle_name" value="{{ old('middle_name') }}" required>

                            @if ($errors->has('middle_name'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    {{--middle name--}}

                    {{--last name--}}
                    <div class="form-group row">
                        <label for="last_name" class="col-md-4 col-form-label">Last Name</label>

                        <div class="col-md-12">
                            <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required>

                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    {{--last name--}}

                    {{-- birthdate --}}
                    <div class="form-group row">

                        <label for="from" class="col-md-4 col-form-label">Birthdate</label>

                        <div class="col-md-12">
                            {{--{{ Form::date('from', \Carbon\Carbon::now()) , ['class' => 'form-control'] }}--}}
                            <input type="text" class="form-control datetimepicker-input" name="birthdate" id="birthdate" data-toggle="datetimepicker" data-target="#birthdate"/>
                            @if ($errors->has('birthdate'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('birthdate') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{-- birthdate --}}

                    {{-- gender --}}
                    <div class="form-group row" id="gender_div">

                        <label for="gender" class="col-md-4 col-form-label">Gender</label>

                        <div class="col-md-12">
                            <select class="form-control" name="gender" id="gender">
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                            @if ($errors->has('gender'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('gender') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{-- gender --}}

                    {{-- school id --}}
                    <div class="form-group row" id="school_id_div">

                        <label for="school_id" class="col-md-4 col-form-label">School</label>

                        <div class="col-md-12">
                            <select class="form-control" name="school_id" id="school_id">
                                @foreach(\App\School::all() as $school)
                                    <option value="{{ $school->id }}">{{ $school->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('school_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('school_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{-- school id --}}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    {{  Form::submit('Submit', ['class' => 'btn btn-primary', 'id' => 'modal_submit']) }}
                </div>
            </div>
        </div>
        </form>
    </div>


    <script type="text/javascript">
        $(document).ready(function() {

            $('select').selectpicker();
            $('#birthdate').datetimepicker({
                viewMode: 'years',
                format: 'YYYY-MM-DD'
            });


            $('#to').datetimepicker({
                viewMode: 'years',
                format: 'YYYY-MM-DD'
            });

            var table = $('#examinees-table').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: '{{ route('examinees.data') }}',
                columns: [
                    { data: 'lrn', name: 'lrn' },
                    { data: 'name', name: 'name' },
                    { data: 'gender', name: 'gender' },
                    { data: 'school', name: 'school' },
                    { data: 'status', name: 'status' },
                    { data: null,
                        render: function ( data, type, row ) {
                            return '<button id="edit" type="button" data-toggle="modal" data-target="#editExaminee" class="btn btn-secondary btn-block edit">Edit</button>' +
                                '<button id="status" type="button" class="btn btn-warning btn-block status">Enable/Disable</button>' +
                                '<button id="delete" type="button" class="btn btn-danger btn-block delete">Delete</button>';
                        }
                    }
                ]
            });

            $("#addExaminee").on('click', function() {
                //Modal tag manipulations
                $("#_method").remove();
                $("#editExamineeTitle").text('Add Examinee')

                //clearing current data values
                $("#lrn").val('');
                $("#last_name").val('');
                $("#first_name").val('');
                $("#middle_name").val('');
                $("#birthdate").val('');

                $("#lrn_div").show();
            });

            $("#examinees-table tbody").on( 'click', 'button.edit', function () {
                var data = table.row( $(this).parents('tr') ).data();

                //Modal tag manipulations
                $('#modal_form').append('<input type="hidden" id="_method" name="_method" value="PUT" />');
                $("#editExamineeTitle").text('Edit Examinee')

                //setting current data values
                $("#id").val(data['id']);
                $("#last_name").val(data['last_name']);
                $("#first_name").val(data['first_name']);
                $("#middle_name").val(data['middle_name']);
                $("#birthdate").val(data['birthdate']);

                $("#gender").val(data['gender']);
                $("#gender_div").find('.filter-option-inner-inner').text(data['gender']);

                $("#school_id").val(data['school_id']);
                $("#school_id_div").find('.filter-option-inner-inner').text(data['school']);

                $("#lrn").val(data['lrn']);
                $("#lrn_div").hide();
            });

            $("#examinees-table tbody").on( 'click', 'button.status', function () {
                swal({
                    title: "Change Status",
                    text: "Are you sure you want to change the status of this data?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((status) => {
                    if (status) {
                        var data = table.row( $(this).parents('tr') ).data();
                        $.ajax({
                            type: "PUT",
                            url: 'examinees/' + data['id'] + '/status',
                            data: {
                                'id' : data['id'],
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(result) {
                                swal("Data successfully updated.", {
                                    icon: "success",
                                });
                                table.ajax.reload();
                            },
                            error: function (error) {
                                swal({
                                    title: "Error",
                                    text: "Error occurred when updating data.",
                                    icon: "error",
                                });
                            }
                        });
                    }
                });
            });

            $("#examinees-table tbody").on( 'click', 'button.delete', function () {
                swal({
                    title: "Delete Data",
                    text: "Are you sure you want to delete this data?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        var data = table.row( $(this).parents('tr') ).data();
                        $.ajax({
                            type: "DELETE",
                            url: 'examinees/' + data['id'] + '/delete',
                            data: {
                                'id' : data['id'],
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(result) {
                                swal("Data successfully deleted.", {
                                    icon: "success",
                                });
                                table.ajax.reload();
                            },
                            error: function (error) {
                                swal({
                                    title: "Error",
                                    text: "Error occurred when deleting data.",
                                    icon: "error",
                                });
                            }
                        });
                    }
                });
            });

            $("#modal_form").submit(function(e){
                if($("#lrn").val() == '' ||
                    $("#last_name").val() == '' ||
                    $("#first_name").val() == '' ||
                    $("#birthdate").val() == '') {
                    swal({
                        title: "Warning",
                        text: "Please fill all inputs.",
                        icon: "warning",
                    });
                    e.preventDefault();
                }

            });


        });

    </script>

@endsection
