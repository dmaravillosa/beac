@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            {{ csrf_field() }}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Manage Registration Periods</div>

                    <div class="card-body">
                        <a class="btn btn-info mb-3" id="addPeriod" data-toggle="modal" data-target="#editPeriod">Create Registration Period</a>
                        <table class="table table-bordered" id="periods-table">
                            <thead>
                            <tr>
                                <th>Batch ID</th>
                                <th>Registration Start</th>
                                <th>Registration End</th>
                                <th>Status</th>
                                <th width="20px">Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Period Modal -->
    <div class="modal fade" id="editPeriod" tabindex="-1" role="dialog" aria-labelledby="editPeriodTitle" aria-hidden="true">
        {!! Form::open(['id' => 'modal_form', 'class' => 'form-horizontal', 'route' => 'periods.edit', disableValidateIfTesting()]) !!}
        {{--{!! method_field('PUT') !!}--}}
        <input type="hidden" name="id" id="id">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editPeriodTitle">Add Registration Period</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- from --}}
                    <div class="form-group row">

                        <label for="from" class="col-md-4 col-form-label">Registration Start</label>

                        <div class="col-md-12">
                            {{--{{ Form::date('from', \Carbon\Carbon::now()) , ['class' => 'form-control'] }}--}}
                            <input type="text" class="form-control datetimepicker-input" name="from" id="from" data-toggle="datetimepicker" data-target="#from"/>
                            @if ($errors->has('from'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('from') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{-- from --}}

                    {{-- to --}}
                    <div class="form-group row">
                        <label for="to" class="col-md-4 col-form-label">Registration End</label>

                        <div class="col-md-12">
                            <input type="text" class="form-control datetimepicker-input" name="to" id="to" data-toggle="datetimepicker" data-target="#to"/>
                            @if ($errors->has('to'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('to') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{-- to --}}



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    {{  Form::submit('Submit', ['class' => 'btn btn-primary', 'id' => 'modal_submit']) }}
                </div>
            </div>
        </div>
        </form>
    </div>


    <script type="text/javascript">
        $(document).ready(function() {

            $('select').selectpicker();
            $('#from').datetimepicker({
                viewMode: 'years',
                format: 'YYYY-MM-DD'
            });


            $('#to').datetimepicker({
                viewMode: 'years',
                format: 'YYYY-MM-DD'
            });

            var table = $('#periods-table').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: '{{ route('periods.data') }}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'from', name: 'from' },
                    { data: 'to', name: 'to' },
                    { data: 'status', name: 'status' },
                    { data: null,
                        render: function ( data, type, row ) {
                            return '<button id="edit" type="button" data-toggle="modal" data-target="#editPeriod" class="btn btn-secondary btn-block edit">Edit</button>' +
                                '<button id="status" type="button" class="btn btn-warning btn-block status">Enable/Disable</button>' +
                                '<button id="delete" type="button" class="btn btn-danger btn-block delete">Delete</button>';
                        }
                    }
                ],
                columnDefs: [
                    {
                        "render": function ( data, type, row ) {
                            // return formatReadableDate(data);
                            return formatDateToReadable(data);
                        },
                        "targets": [1,2]
                    },
                ]

            });

            $("#addPeriod").on('click', function() {
                //Modal tag manipulations
                $("#_method").remove();
                $("#editPeriodTitle").text('Add Registration Period')

                //clearing current data values
                $("#from").val('');
                $("#to").val('');
            });

            $("#periods-table tbody").on( 'click', 'button.edit', function () {
                var data = table.row( $(this).parents('tr') ).data();

                //Modal tag manipulations
                $('#modal_form').append('<input type="hidden" id="_method" name="_method" value="PUT" />');
                $("#editPeriodTitle").text('Edit Registration Period')

                //setting current data values
                $("#from").val(data['from']);
                $("#to").val(data['to']);
                $("#id").val(data['id']);
            });

            $("#periods-table tbody").on( 'click', 'button.status', function () {
                swal({
                    title: "Change Status",
                    text: "Are you sure you want to change the status of this data?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((status) => {
                    if (status) {
                        var data = table.row( $(this).parents('tr') ).data();
                        $.ajax({
                            type: "PUT",
                            url: 'periods/' + data['id'] + '/status',
                            data: {
                                'id' : data['id'],
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(result) {
                                swal("Data successfully updated.", {
                                    icon: "success",
                                });
                                table.ajax.reload();
                            },
                            error: function (error) {
                                swal({
                                    title: "Error",
                                    text: "Error occurred when updating data.",
                                    icon: "error",
                                });
                            }
                        });
                    }
                });
            });

            $("#periods-table tbody").on( 'click', 'button.delete', function () {
                swal({
                    title: "Delete Data",
                    text: "Are you sure you want to delete this data?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        var data = table.row( $(this).parents('tr') ).data();
                        $.ajax({
                            type: "DELETE",
                            url: 'periods/' + data['id'] + '/delete',
                            data: {
                                'id' : data['id'],
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(result) {
                                swal("Data successfully deleted.", {
                                    icon: "success",
                                });
                                table.ajax.reload();
                            },
                            error: function (error) {
                                swal({
                                    title: "Error",
                                    text: "Error occurred when deleting data.",
                                    icon: "error",
                                });
                            }
                        });
                    }
                });
            });


        });

    </script>

@endsection
