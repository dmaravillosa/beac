@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            {{--OFFICIAL LIST OF EXAMINEES--}}
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">Official List of Examinees</div>

                    <div class="card-body">
                        {!! Form::open(['id' => 'modal_form', 'class' => 'form-horizontal', 'route' => 'examinees.list', disableValidateIfTesting()]) !!}

                        <div class="form-group">
                            <label for="cluster" class="col-md-4 col-form-label">Cluster</label>
                            <div class="col-md-12">
                                <select name="cluster_id" id="cluster" class="form-control">
                                    @foreach(\App\Cluster::all() as $cluster)
                                        <option value="{{ $cluster->id }}">{{ $cluster->cluster }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <button class="btn btn-info mb-3" type="submit" id="list_button">Generate Report</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>


            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">Registration Summary Report</div>

                    <div class="card-body">
                        {!! Form::open(['id' => 'modal_form', 'class' => 'form-horizontal', 'route' => 'examinees.list', disableValidateIfTesting()]) !!}

                        <div class="form-group">
                            <label for="cluster" class="col-md-4 col-form-label">Cluster</label>
                            <div class="col-md-12">
                                <select name="cluster_id" id="cluster" class="form-control">
                                    @foreach(\App\Cluster::all() as $cluster)
                                        <option value="{{ $cluster->id }}">{{ $cluster->cluster }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <button class="btn btn-info mb-3" type="submit" id="list_button">Generate Report</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {

            $("#list_button").click(function () {
                swal({
                    title: "Please Wait",
                    text: "Generating Report...",
                    icon: "info",
                    button: false,
                    closeOnClickOutside: false,
                });
            });

        });
    </script>
@endsection
