@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            {{ csrf_field() }}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Manage Schools</div>

                    <div class="card-body">
                        <a class="btn btn-info mb-3" id="addSchool" data-toggle="modal" data-target="#editSchool">Create School</a>
                        <table class="table table-bordered" id="schools-table">
                            <thead>
                            <tr>
                                <th>School Name</th>
                                <th>Head</th>
                                <th>Designation</th>
                                <th>Address</th>
                                <th>School Type</th>
                                <th>Classification</th>
                                <th>Division</th>
                                <th>Cluster Code</th>
                                <th>Status</th>
                                <th width="20px">Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit School Modal -->
    <div class="modal fade" id="editSchool" tabindex="-1" role="dialog" aria-labelledby="editSchoolTitle" aria-hidden="true">
        {!! Form::open(['id' => 'modal_form', 'class' => 'form-horizontal', 'route' => 'schools.edit', disableValidateIfTesting()]) !!}
        {{--{!! method_field('PUT') !!}--}}
        <input type="hidden" name="id" id="id">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editSchoolTitle">Add School</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{--School Name--}}
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label">School Name</label>

                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{--School Name--}}

                    {{--Head--}}
                    <div class="form-group row">
                        <label for="head" class="col-md-4 col-form-label">School Head</label>

                        <div class="col-md-12">
                            <input id="head" type="text" class="form-control{{ $errors->has('head') ? ' is-invalid' : '' }}" name="head" value="{{ old('head') }}" required>

                            @if ($errors->has('head'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('head') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{--Head--}}

                    {{--designation--}}
                    <div class="form-group row">
                        <label for="designation" class="col-md-4 col-form-label">Designation</label>

                        <div class="col-md-12">
                            <input id="designation" type="text" class="form-control{{ $errors->has('designation') ? ' is-invalid' : '' }}" name="designation" value="{{ old('designation') }}" required>

                            @if ($errors->has('designation'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('designation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{--designation--}}

                    {{--address--}}
                    <div class="form-group row">
                        <label for="address" class="col-md-4 col-form-label">Address</label>

                        <div class="col-md-12">
                            <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" required>

                            @if ($errors->has('address'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{--address--}}

                    {{-- school_type --}}
                    <div class="form-group row" id="school_type_div">
                        <label for="school_type" class="col-md-4 col-form-label">School Type</label>

                        <div class="col-md-12">
                            {{--{{ Form::select('cluster_id', \App\Cluster::all()->pluck('cluster'), \App\Cluster::where('id', '3')->pluck('id')->first(), ['placeholder'=>'Pick a cluster..', 'class' => 'form-control' . ($errors->has('cluster_id') ? ' is-invalid' : '')]) }}--}}
                            <select class="form-control {{ $errors->has('school_type') ? ' is-invalid' : '' }}" id="school_type" name="school_type[]" multiple="multiple">
                                @foreach(\App\SchoolType::select('id','name')->get() as $school_type)
                                    <option value="{{ $school_type->id }}">{{ $school_type->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('school_type'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('school_type') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{-- school_type --}}

                    {{-- classification --}}
                    <div class="form-group row" id="classification_div">
                        <label for="classification" class="col-md-4 col-form-label">Classification</label>

                        <div class="col-md-12">
                            {{--{{ Form::select('cluster_id', \App\Cluster::all()->pluck('cluster'), \App\Cluster::where('id', '3')->pluck('id')->first(), ['placeholder'=>'Pick a cluster..', 'class' => 'form-control' . ($errors->has('cluster_id') ? ' is-invalid' : '')]) }}--}}
                            <select class="form-control {{ $errors->has('classification') ? ' is-invalid' : '' }}" id="classification" name="classification">
                                <option value="Public">Public</option>
                                <option value="Private">Private</option>
                            </select>
                            @if ($errors->has('classification'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('classification') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{-- classification --}}

                    {{-- division --}}
                    <div class="form-group row" id="division_id_div">
                        <label for="division_id" class="col-md-4 col-form-label">Division</label>

                        <div class="col-md-12">
                            <select class="form-control {{ $errors->has('division_id') ? ' is-invalid' : '' }}" id="division_id" name="division_id">
                                @foreach(\App\Division::all() as $division)
                                    <option value="{{ $division->id }}">{{ $division->division }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('division_id'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('division_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{-- division --}}

                    {{-- Cluster Code --}}
                    <div class="form-group row" id="cluster_div">
                        <label for="cluster_id" class="col-md-4 col-form-label">Cluster Code</label>

                        <div class="col-md-12">
                            {{--{{ Form::select('cluster_id', \App\Cluster::all()->pluck('cluster'), \App\Cluster::where('id', '3')->pluck('id')->first(), ['placeholder'=>'Pick a cluster..', 'class' => 'form-control' . ($errors->has('cluster_id') ? ' is-invalid' : '')]) }}--}}
                            <select class="form-control {{ $errors->has('cluster_id') ? ' is-invalid' : '' }}" id="cluster_id" name="cluster_id">
                                @foreach(\App\Cluster::all() as $cluster)
                                    <option value="{{ $cluster->id }}">{{ $cluster->cluster_code . ' (' .$cluster->cluster . ')' }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('cluster_id'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('cluster_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{-- Cluster Code --}}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    {{  Form::submit('Submit', ['class' => 'btn btn-primary', 'id' => 'modal_submit']) }}
                </div>
            </div>
        </div>
        </form>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {

            $('select').selectpicker();

            var table = $('#schools-table').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: '{{ route('schools.data') }}',
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'head', name: 'head' },
                    { data: 'designation', name: 'designation' },
                    { data: 'address', name: 'address' },
                    { data: 'school_type', name: 'school_type' },
                    { data: 'classification', name: 'classification' },
                    { data: 'division', name: 'division' },
                    { data: 'cluster_code', name: 'cluster_code' },
                    { data: 'status', name: 'status' },
                    { data: null,
                        render: function ( data, type, row ) {
                            return '<button id="edit" type="button" data-toggle="modal" data-target="#editSchool" class="btn btn-secondary btn-block edit">Edit</button>' +
                                '<button id="status" type="button" class="btn btn-warning btn-block status">Enable/Disable</button>' +
                                '<button id="delete" type="button" class="btn btn-danger btn-block delete">Delete</button>';
                        }
                    }
                ]
            });

            $("#addSchool").on('click', function() {
                //Modal tag manipulations
                $("#_method").remove();
                $("#editSchoolTitle").text('Add School');

                //clearing current data values
                $("#name").val('');
                $("#head").val('');
                $("#designation").val('');
                $("#address").val('');

                //clear data on select tags
                $("#school_type_div").find('.filter-option-inner-inner').text('Nothing selected');
            });

            $("#schools-table tbody").on( 'click', 'button.edit', function () {

                var data = table.row( $(this).parents('tr') ).data();

                //Modal tag manipulations
                $('#modal_form').append('<input type="hidden" id="_method" name="_method" value="PUT" />');
                $("#editSchoolTitle").text('Edit School');

                //setting current data values
                $("#id").val(data['id']);
                $("#name").val(data['name']);
                $("#head").val(data['head']);
                $("#designation").val(data['designation']);
                $("#address").val(data['address']);

                //set selected on modal select tags
                $("#classification").val(data['classification']);
                $("#classification_div").find('.filter-option-inner-inner').text(data['classification']);
                $("#cluster_id").val(data['cluster_id']);
                $("#cluster_div").find('.filter-option-inner-inner').text(data['cluster_code']);

                $("#division_id").val(data['division']);
                $("#division_id_div").find('.filter-option-inner-inner').text(data['division']);

                $("#school_type").val('test');
                $("#school_type_div").find('.filter-option-inner-inner').text(data['school_type']);
                $.each( data["school_type"].split(','), function( key, value ) {
                    $('#school_type option[value="' + $.trim(value) + '"]').prop('selected', true);
                });
            });

            $("#schools-table tbody").on( 'click', 'button.status', function () {
                swal({
                    title: "Change Status",
                    text: "Are you sure you want to change the status of this data?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((status) => {
                    if (status) {
                        var data = table.row( $(this).parents('tr') ).data();
                        $.ajax({
                            type: "PUT",
                            url: 'schools/' + data['id'] + '/status',
                            data: {
                                'id' : data['id'],
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(result) {
                                swal("Data successfully updated.", {
                                    icon: "success",
                                });
                                table.ajax.reload();
                            },
                            error: function (error) {
                                swal({
                                    title: "Error",
                                    text: "Error occurred when updating data.",
                                    icon: "error",
                                });
                            }
                        });
                    }
                });
            });

            $("#schools-table tbody").on( 'click', 'button.delete', function () {
                swal({
                    title: "Delete Data",
                    text: "Are you sure you want to delete this data?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        var data = table.row( $(this).parents('tr') ).data();
                        $.ajax({
                            type: "DELETE",
                            url: 'schools/' + data['id'] + '/delete',
                            data: {
                                'id' : data['id'],
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(result) {
                                swal("Data successfully deleted.", {
                                    icon: "success",
                                });
                                table.ajax.reload();
                            },
                            error: function (error) {
                                swal({
                                    title: "Error",
                                    text: "Error occurred when deleting data.",
                                    icon: "error",
                                });
                            }
                        });
                    }
                });
            });

            //On Modal CLose
            $('#editSchool').on('hidden.bs.modal', function () {
                clearError();
            })

            $("#modal_form").submit(function(e){
                if($("#name").val() == '' ||
                    $("#head").val() == '' ||
                    $("#designation").val() == '' ||
                    $("#address").val() == '') {
                    swal({
                        title: "Warning",
                        text: "Please fill all inputs.",
                        icon: "warning",
                    });
                    e.preventDefault();
                }

            });

        });

    </script>

@endsection