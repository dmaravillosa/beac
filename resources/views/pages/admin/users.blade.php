@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            {{ csrf_field() }}
            <div class="col-md-13">
                <div class="card">
                    <div class="card-header">Manage Users</div>

                    <div class="card-body">
                        <a class="btn btn-info mb-3" href="register">Create User</a>
                        <table class="table table-bordered" id="users-table">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Middle Name</th>
                                <th>Last Name</th>
                                <th>Designation</th>
                                <th>School Type</th>
                                <th>Classification</th>
                                <th>Region Access</th>
                                <th>Division Access</th>
                                <th>Created By</th>
                                <th>Status</th>
                                <th width="20px">Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit User Modal -->
    <div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="editUserTitle" aria-hidden="true">
        {!! Form::open(['class' => 'form-horizontal', 'route' => 'users.edit', disableValidateIfTesting()]) !!}
        {!! method_field('PUT') !!}
        <input type="hidden" name="id" id="id">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editUserTitle">Edit User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{--first name--}}
                        <div class="form-group row">
                            <label for="first_name" class="col-md-4 col-form-label">First Name</label>

                            <div class="col-md-12">
                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required>

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{--first name--}}

                        {{--middle name--}}
                        <div class="form-group row">
                            <label for="middle_name" class="col-md-4 col-form-label">Middle Name</label>

                            <div class="col-md-12">
                                <input id="middle_name" type="text" class="form-control{{ $errors->has('middle_name') ? ' is-invalid' : '' }}" name="middle_name" value="{{ old('middle_name') }}" required>

                                @if ($errors->has('middle_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{--middle name--}}

                        {{--last name--}}
                        <div class="form-group row">
                            <label for="last_name" class="col-md-4 col-form-label">Last Name</label>

                            <div class="col-md-12">
                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required>

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{--last name--}}

                        {{--designation--}}
                        <div class="form-group row">
                            <label for="designation" class="col-md-4 col-form-label">Designation</label>

                            <div class="col-md-12">
                                <input id="designation" type="text" class="form-control{{ $errors->has('designation') ? ' is-invalid' : '' }}" name="designation" value="{{ old('designation') }}" required>

                                @if ($errors->has('designation'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('designation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{--designation--}}

                        {{--School Type--}}
                        {{--<div class="form-group row">--}}
                            {{--<label for="school_type" class="col-md-4 col-form-label">School Type</label>--}}

                            {{--<div class="col-md-12">--}}
                                {{--{!! Form::select('school_type[]', \App\SchoolType::all()->pluck('name'), null, ['class' => 'selectpicker form-control' . ($errors->has('role') ? ' is-invalid' : ''), 'multiple']) !!}--}}
                                {{--@if ($errors->has('school_type'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('school_type') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--School Type--}}

                        {{-- Classification --}}
                        {{--<div class="form-group row">--}}
                            {{--<label for="classification" class="col-md-4 col-form-label">Classification</label>--}}

                            {{--<div class="col-md-12">--}}
                                {{--{{ Form::select('classification', ['public' => 'Public', 'private' => 'Private'], old('classification') ,[ 'class' => 'form-control' . ($errors->has('classification') ? ' is-invalid' : '')]) }}--}}
                                {{--@if ($errors->has('classification'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('classification') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{-- Classification --}}

                        {{-- Region Access --}}
                        {{--<div class="form-group row">--}}
                            {{--<label for="region_access" class="col-md-4 col-form-label">Region Access</label>--}}

                            {{--<div class="col-md-12">--}}
                                {{--{{ Form::select('region_access', \App\Region::all()->pluck('code'), null, ['placeholder'=>'Pick a region..', 'class' => 'form-control' . ($errors->has('region_access') ? ' is-invalid' : '')]) }}--}}
                                {{--@if ($errors->has('region_access'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('region_access') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{-- Region Access --}}

                        {{-- Division Access --}}
                        {{--<div class="form-group row">--}}
                            {{--<label for="division_access" class="col-md-4 col-form-label">Division Access</label>--}}

                            {{--<div class="col-md-12">--}}
                                {{--<input id="division_access" type="text" class="form-control{{ $errors->has('division_access') ? ' is-invalid' : '' }}" name="division_access" value="{{ old('division_access') }}" required>--}}
                                {{--@if ($errors->has('division_access'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('division_access') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{-- Division Access --}}


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        {{  Form::submit('Update', ['class' => 'btn btn-primary']) }}
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {

            $('select').selectpicker();

            var table = $('#users-table').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: '{{ route('users.data') }}',
                columns: [
                    { data: 'first_name', name: 'first_name' },
                    { data: 'middle_name', name: 'middle_name' },
                    { data: 'last_name', name: 'last_name' },
                    { data: 'designation', name: 'designation' },
                    { data: 'school_type', name: 'school_type' },
                    { data: 'classification', name: 'classification' },
                    { data: 'region_access', name: 'region_access' },
                    { data: 'division_access', name: 'division_access' },
                    { data: 'created_by', name: 'created_by' },
                    { data: 'status', name: 'status' },
                    { data: null,
                        render: function ( data, type, row ) {
                            return '<button id="edit" type="button" data-toggle="modal" data-target="#editUser" class="btn btn-secondary btn-block edit">Edit</button>' +
                                '<button id="status" type="button" class="btn btn-warning btn-block status">Enable/Disable</button>' +
                                '<button id="delete" type="button" class="btn btn-danger btn-block delete">Delete</button>';
                        }
                    }
                ]
            });

            $('#users-table tbody').on( 'click', 'button.edit', function () {
                var data = table.row( $(this).parents('tr') ).data();
                $("#id").val(data['id']);
                $("#first_name").val(data['first_name']);
                $("#middle_name").val(data['middle_name']);
                $("#last_name").val(data['last_name']);
                $("#designation").val(data['designation']);
                // $("#school_type").val(data['school_type']);
                // $("#region_access").val(data['region_access']);
                // $("#division_access").val(data['division_access']);
            });

            $('#users-table tbody').on( 'click', 'button.status', function () {
                swal({
                    title: "Change Status",
                    text: "Are you sure you want to change the status of this account?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((status) => {
                    if (status) {
                        var data = table.row( $(this).parents('tr') ).data();
                        $.ajax({
                            type: "PUT",
                            url: 'users/' + data['id'] + '/status',
                            data: {
                                'id' : data['id'],
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(result) {
                                swal("Account successfully updated.", {
                                    icon: "success",
                                });
                                table.ajax.reload();
                            },
                            error: function (error) {
                                swal({
                                    title: "Error",
                                    text: "Error occurred when updating user.",
                                    icon: "error",
                                });
                            }
                        });
                    }
                });
            });

            $('#users-table tbody').on( 'click', 'button.delete', function () {
                swal({
                    title: "Delete Account",
                    text: "Are you sure you want to delete this account?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        var data = table.row( $(this).parents('tr') ).data();
                        $.ajax({
                            type: "DELETE",
                            url: 'users/' + data['id'] + '/delete',
                            data: {
                                'id' : data['id'],
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(result) {
                                swal("Account successfully deleted.", {
                                    icon: "success",
                                });
                                table.ajax.reload();
                            },
                            error: function (error) {
                                swal({
                                    title: "Error",
                                    text: "Error occurred when deleting user.",
                                    icon: "error",
                                });
                            }
                        });
                    }
                });

            });

        });

    </script>

@endsection
