@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Profile</div>

                    @if (session('alert'))
                        <div id="alertdiv" class="alert alert-success">
                            {{ session('alert') }}
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 px-0">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <img src="{{ asset(Config::get('constants.profile_image') . Auth::user()->image) }}" class="img-thumbnail"/>
                                        </div>
                                        <div class="col-md-12 mt-3">
                                            <button class="btn btn-primary btn-block">Update Picture</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8 px-0">
                                {!! Form::model(Auth::user(), ['class' => 'form-horizontal', 'action' => ['UsersController@update', Auth::user()->id], disableValidateIfTesting()]) !!}
                                {!! method_field('PUT') !!}
                                {{ Form::hidden('id') }}
                                    <!-- start: first name -->
                                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                            {!! Form::label('first_name', 'First Name', ['class' => 'col-sm-4 control-label']) !!}
                                            <div class="col-sm-10">
                                                {!! Form::text('first_name', $errors->has('first_name') ? old('first_name') : Auth::user()->first_name,
                                                ['class' => 'form-control', 'maxlength' => empty(disableValidateIfTesting()) ? '100' : '', 'required']) !!}
                                                @if ($errors->has('first_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    <!-- end: first name -->

                                    <!-- start: middle name -->
                                    <div class="form-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                                        {!! Form::label('middle_name', 'Middle Name', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="col-sm-10">
                                            {!! Form::text('middle_name', $errors->has('middle_name') ? old('middle_name') : Auth::user()->middle_name,
                                            ['class' => 'form-control', 'maxlength' => empty(disableValidateIfTesting()) ? '100' : '', 'required']) !!}
                                            @if ($errors->has('middle_name'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- end: middle name -->

                                    <!-- start: last name -->
                                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        {!! Form::label('last_name', 'Last Name', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="col-sm-10">
                                            {!! Form::text('last_name', $errors->has('last_name') ? old('last_name') : Auth::user()->last_name,
                                            ['class' => 'form-control', 'maxlength' => empty(disableValidateIfTesting()) ? '100' : '', 'required']) !!}
                                            @if ($errors->has('last_name'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- end: last name -->

                                    <!-- start: email -->
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        {!! Form::label('email', 'Email Address', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="col-sm-10">
                                            {!! Form::text('email', $errors->has('email') ? old('email') : Auth::user()->email,
                                            ['class' => 'form-control', 'maxlength' => empty(disableValidateIfTesting()) ? '100' : '', 'required']) !!}
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- end: email -->

                                    <!-- start: button -->
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <div class="col-sm-10 mt-4">
                                            {{  Form::submit('Update', ['class' => 'btn btn-primary']) }}
                                            <a class="btn btn-outline-primary" href="home" role="button">Cancel</a>
                                        </div>
                                    </div>
                                    <!-- end: button -->


                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $('#alertdiv').delay(3000).slideUp(300);
    </script>
@endsection

