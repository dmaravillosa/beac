<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$this->get('/', 'Auth\LoginController@showLoginForm');

// Login Routes
Auth::routes();

// Role Redirect
$this->get('/home', 'HomeController@index')->name('home');

// profile
$this->group(['prefix' => 'profile'], function () {
    $this->get('/', 'ProfileController@index')->name('profile');

    $this->group(['prefix' => '{id}'], function () {
        $this->put('/reset', 'ProfileController@resetPassword')->name('reset');
    });
});

// users
$this->group(['prefix' => 'users'], function () {

    // users
    $this->get('/', 'UsersController@index')->name('users');
    $this->put('/', 'UsersController@update')->name('users.edit');
    $this->get('/data', 'UsersController@data')->name('users.data');

    // users/{id}
    $this->group(['prefix' => '{id}'], function () {
        $this->put('/', 'UsersController@update');
        $this->put('/status', 'UsersController@updateStatus');
        $this->delete('/delete', 'UsersController@destroy');
    });

});

//Assessment Center Module
$this->group(['prefix' => 'centers'], function(){

    $this->get('/', 'CentersController@index')->name('centers');
    $this->post('/', 'CentersController@store')->name('centers.add');
    $this->put('/', 'CentersController@update')->name('centers.edit');
    $this->get('/data', 'CentersController@data')->name('centers.data');

    $this->group(['prefix' => '{id}'], function () {
        $this->put('/', 'CentersController@update');
        $this->put('/status', 'CentersController@updateStatus');
        $this->delete('/delete', 'CentersController@destroy');
    });

});

//Schools Module
$this->group(['prefix' => 'schools'], function(){

    $this->get('/', 'SchoolsController@index')->name('schools');
    $this->post('/', 'SchoolsController@store')->name('schools.add');
    $this->put('/', 'SchoolsController@update')->name('schools.edit');
    $this->get('/data', 'SchoolsController@data')->name('schools.data');

    $this->group(['prefix' => '{id}'], function () {
        $this->put('/', 'SchoolsController@update');
        $this->put('/status', 'SchoolsController@updateStatus');
        $this->delete('/delete', 'SchoolsController@destroy');
    });

});

//Schools Module
$this->group(['prefix' => 'periods'], function(){

    $this->get('/', 'PeriodsController@index')->name('periods');
    $this->post('/', 'PeriodsController@store')->name('periods.add');
    $this->put('/', 'PeriodsController@update')->name('periods.edit');
    $this->get('/data', 'PeriodsController@data')->name('periods.data');

    $this->group(['prefix' => '{id}'], function () {
        $this->put('/', 'PeriodsController@update');
        $this->put('/status', 'PeriodsController@updateStatus');
        $this->delete('/delete', 'PeriodsController@destroy');
    });

});

//Examinees Module
$this->group(['prefix' => 'examinees'], function(){

    $this->get('/', 'ExamineeController@index')->name('examinees');
    $this->post('/', 'ExamineeController@store')->name('examinees.add');
    $this->put('/', 'ExamineeController@update')->name('examinees.edit');
    $this->get('/data', 'ExamineeController@data')->name('examinees.data');
    $this->get('/examinee-form', 'ExamineeController@form')->name('examinees.form');
    $this->post('/examinee-list', 'ExamineeController@list')->name('examinees.list');

    $this->group(['prefix' => '{id}'], function () {
        $this->put('/', 'ExamineeController@update');
        $this->put('/status', 'ExamineeController@updateStatus');
        $this->delete('/delete', 'ExamineeController@destroy');
    });

});

// Records Module
$this->get('/records', 'RecordsController@index')->name('records');


// Reports Module
$this->get('/reports', 'ReportsController@index')->name('reports');


// Students Module
$this->get('/students', 'StudentsController@index')->name('students');


//Answers Module
$this->get('/answers', 'AnswersController@index')->name('answers');

