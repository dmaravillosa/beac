<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminTest extends DuskTestCase
{
    /**
     * @throws \Throwable
     *
     * Test redirect to admin page
     */
    public function test_admin_redirect()
    {

        $this->browse(function ($browser) {
            $browser->visit('/')
                    ->type('email', 'johndoe@gmail.com')
                    ->type('password', 'secret')
                    ->press('Login')
                    ->assertSee('Admin Access')
                    ->assertSee('Examinee Records')
                    ->assertSee('Reports');
        });
    }

    /**
     * @throws \Throwable
     *
     * Test redirect to admin page
     */
    public function test_student_records_link()
    {
        $this->browse(function ($browser) {
            $browser->visit('/home')
                ->clickLink('Examinee Records')
                ->assertSee('Manage Examinees');
        });
    }

    /**
     * @throws \Throwable
     *
     * Test redirect to admin page
     */
    public function test_reports_link()
    {
        $this->browse(function ($browser) {
            $browser->visit('/home')
                ->clickLink('Reports')
                ->assertSee('Official List of Examinees')
                ->assertSee('Registration Summary Report');
        });
    }
}
