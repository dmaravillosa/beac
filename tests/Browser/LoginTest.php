<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * Test all errors
     */
    public function test_show_all_error(){
        $this->browse(function ($browser) {
            $browser->visit('/login')
                ->press('Login')
                ->assertSee('The email field is required.')
                ->assertSee('The password field is required.');
        });
    }

    /**
     * Test email required error
     */
    public function test_email_required_error(){
        $this->browse(function ($browser) {
            $browser->visit('/login')
                ->type('password', 'secret')
                ->press('Login')
                ->assertSee('The email field is required.');
        });
    }

    /**
     * Test password required error
     */
    public function test_password_required_error(){
        $this->browse(function ($browser) {
            $browser->visit('/login')
                ->type('email', 'test@gmail.com')
                ->press('Login')
                ->assertSee('The password field is required.');
        });
    }

    /**
     * Test credential does not match error
     */
    public function test_credential_does_not_match_error(){
        $this->browse(function ($browser) {
            $browser->visit('/login')
                ->type('email', 'johndoe@gmail.com')
                ->type('password', 'test')
                ->press('Login')
                ->assertSee('These credentials do not match our records.');
        });
    }


    /**
     * Test login using admin account
     */
    public function test_login_admin()
    {
        $user = User::where('email', 'johndoe@gmail.com')->first();

        $this->browse(function ($browser) use ($user) {
            $browser->visit('/login')
                ->type('email', $user->email)
                ->type('password', 'secret')
                ->press('Login')
                ->assertSee('Examinee Records')
                ->logout();
        });
    }

    /**
     * Test login using supervisor account
     */
    public function test_login_supervisor()
    {
        $user = User::where('email', 'janedoe@gmail.com')->first();

        $this->browse(function ($browser) use ($user) {
            $browser->visit('/login')
                ->type('email', $user->email)
                ->type('password', 'secret')
                ->press('Login')
                ->assertSee('Manage Students')
                ->logout();
        });
    }

    /**
     * Test login using staff account
     */
    public function test_login_staff()
    {
        $user = User::where('email', 'jackdoe@gmail.com')->first();

        $this->browse(function ($browser) use ($user) {
            $browser->visit('/login')
                ->type('email', $user->email)
                ->type('password', 'secret')
                ->press('Login')
                ->assertSee('Manage Answer Sheets')
                ->logout();
        });
    }
}
