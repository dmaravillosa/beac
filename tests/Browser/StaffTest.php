<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class StaffTest extends DuskTestCase
{
    /**
     * @throws \Throwable
     *
     * Test redirect to admin page
     */
    public function test_supervisor_redirect()
    {

        $this->browse(function ($browser) {
            $browser->visit('/')
                ->type('email', 'jackdoe@gmail.com')
                ->type('password', 'secret')
                ->press('Login')
                ->assertSee('Staff Access')
                ->assertSee('Manage Answer Sheets');
        });
    }

    /**
     * @throws \Throwable
     *
     * Test redirect to admin page
     */
    public function test_student_records_link()
    {
        $this->browse(function ($browser) {
            $browser->visit('/home')
                ->clickLink('Manage Answer Sheets')
                ->assertSee('Answer Sheets');
        });
    }
}
