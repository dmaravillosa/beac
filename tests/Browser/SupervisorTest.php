<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SupervisorTest extends DuskTestCase
{
    /**
     * @throws \Throwable
     *
     * Test redirect to admin page
     */
    public function test_supervisor_redirect()
    {

        $this->browse(function ($browser) {
            $browser->visit('/')
                ->type('email', 'janedoe@gmail.com')
                ->type('password', 'secret')
                ->press('Login')
                ->assertSee('Supervisor Access')
                ->assertSee('Manage Student');
        });
    }

    /**
     * @throws \Throwable
     *
     * Test redirect to admin page
     */
    public function test_student_records_link()
    {
        $this->browse(function ($browser) {
            $browser->visit('/home')
                ->clickLink('Manage Students')
                ->assertSee('View Students');
        });
    }
}
